# Project Title

_go-gurl_ (Go Get URL), curl alternative, written in go-lang

## Description

This project started as a lark, but started seeming like a worthwhile repository for some ideas born out of prior go development experiences.  Perhaps I can capture some worthwhile primitive patterns here while simultaneously producing a platform independent version of curl that I (and hopefully others) enjoy using.

## Getting Started

### Dependencies

No external dependencies

### Executing program

```shell
go-gurl version 1.0.0a
Usage: go-gurl [options...] <url>
  -a, --accept strings       Accept headers (comma separated) (default [*/*])
  -d, --data string          HTTP POST data
  -f, --fail                 Fail silently (no output at all) on HTTP errors
  -H, --header strings       Pass custom headers to server <key: value> (default [Content-Type: application/json])
  -h, --help                 help
  -X, --request string       HTTP request method: GET, POST, PUT, DELETE (default "GET")
  -s, --silent               Silent mode
  -T, --upload-file string   Transfer local FILE to destination
  -A, --user-agent string    Send User-Agent <name> to server (default "go-gurl")
  -v, --verbose              verbose mode
```

## Help

```shell
go-gurl -h
```

## Authors

Contributors names and contact info

Jonathan Machen  

## Version History

* 1.0.0a - alpha version, not yet feature complete

## License

This project is licensed under the MIT License - see the LICENSE.md file for details
