module main

go 1.22

require (
	github.com/spf13/pflag v1.0.5
	github.com/valyala/fasthttp v1.52.0
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/klauspost/compress v1.17.6 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
